/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.1 from 27.10.2020
  * @author Tim Dunker
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8  ;
    
    // Anzahl der Sterne in unserer Milchstraße
    String anzahlSterne = "200 Milliarden";
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 5855;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = 190000;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
      int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
      int flaecheKleinsteLand = 2;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tieres: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand + "m�");
    
    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + "m�");
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

