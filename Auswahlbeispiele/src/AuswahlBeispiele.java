
public class AuswahlBeispiele {

	public static void main(String[] args) {

		int z1 = 5;
		int z2 = 7;
		int erg = min(z2, z1);
		System.out.println("Ergebnis: " + erg);
		erg = max(z2, z1);
		System.out.println("Ergebnis: " + erg);
	}

	public static int min(int z1, int z2) {
		if (z1 < z2) {
			return z1;
		} else {
			return z2;
		}
	}
	public static int max(int z1, int z2) {
		if (z1 > z2) {
			return z1;
		} else {
			return z2;
		}
}
}
