import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		
		String artikel = liesString("was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

		
	}
  public static String liesString(String text) {
	  System.out.println(text);
	  Scanner myScanner = new Scanner(System.in);
	  String erg = myScanner.next();
	  return erg;
  }
  
  public static int liesInt(String text) {
	  System.out.println(text);
	  Scanner myScanner = new Scanner(System.in);
	  int erg = myScanner.nextInt();
	  return erg;
  }
  
  public static double liesDouble(String text) {
	  System.out.println(text);
	  Scanner myScanner = new Scanner(System.in);
	  Double erg = myScanner.nextDouble();
	  return erg;
  }
  
  public static double berechneGesamtnettopreis(int anzahl, double preis) {
	double erg = anzahl * preis;
	return erg;
  }
  
  public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
	  double erg = nettogesamtpreis * (1 + mwst / 100);
	  return erg;
  }
  
  public static void rechungausgeben(String artikel, int anzahl, double
		  nettogesamtpreis, double bruttogesamtpreis,
		  double mwst) {
	  System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	  
  }

}
