import java.util.Scanner;

public class ABSchleifen2Aufgabe1 {

	public static void main(String[] args) {
		// Aufgabe A
		aufgabeA();
		// Aufgabe B
		aufgabeB();
	}

	public static void aufgabeA() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int eingabeZahl = tastatur.nextInt();

		int zaehler = 1;

		while (zaehler <= eingabeZahl) {
			System.out.print(zaehler + " ");
			zaehler++;
		}

	}

	public static void aufgabeB() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int eingabeZahl = tastatur.nextInt();

		int zaehler = eingabeZahl;

		while (zaehler >= 1) {
			System.out.print(zaehler + " ");
			zaehler--;
		}
		tastatur.close();

	}
}
