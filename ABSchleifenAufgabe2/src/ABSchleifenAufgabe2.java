import java.util.Scanner;

public class ABSchleifenAufgabe2 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int eingabeZahl = tastatur.nextInt();

		int zaehler = 1;
		long sum = 1;
		if (eingabeZahl <= 20) {
			while (zaehler <= eingabeZahl) {
				System.out.print(zaehler);
				if (zaehler == eingabeZahl) {
					System.out.print(" = ");
				} else {
					System.out.print(" * ");
				}
				sum = sum * zaehler;
				zaehler++;
			}
			System.out.print(sum);
			tastatur.close();
		}
	}

}