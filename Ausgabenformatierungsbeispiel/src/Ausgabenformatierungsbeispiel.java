
public class Ausgabenformatierungsbeispiel {

	public static void main(String[] args) {
		
/*			System.out.println("123456789");
			System.out.print("123456789\n");
			System.out.println("123456789");
*/
/*			System.out.printf("|%s|%n","123456789");
			System.out.printf("|%20s|%n","123456789");
			System.out.printf("|%-20s|%n","123456789");
			System.out.printf("|%.3s|%n","123456789");
*/
//			System.out.printf("|%20d|%n",123456789);
//			System.out.printf("|%-20d|%n",123456789);
//			System.out.printf("|%20d|%n",123456789);
			
			System.out.printf("|%20f|%n",123456.789);
			System.out.printf("|%-20f|%n",123456.789);
			System.out.printf("|%20f|%n",-123456.789);
			System.out.printf("|%-20f|%n",-123456.789);
			
//			System.out.printf("Der Preis vom %s ist %.2f Euro. %n", "Monitor" , 109.50);
			
			int wert = 1;
			String w�hrung = "Euro";
			System.out.printf("%8s\n","* * *");
			System.out.printf("%2s%8s\n","*", "*");
			System.out.printf("%1s%5d%5s\n","*",wert,"*");
			System.out.printf("%1s%7s%3s\n","*",w�hrung,"*");
			System.out.printf("%2s%8s\n","*", "*");
			System.out.printf("%8s\n","* * *");
	}	
}
