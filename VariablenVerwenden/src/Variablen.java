﻿

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author Tim Dunker
    @version 1.1
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int programmDurchlaeufe;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  int programmdurchlaeufe = 25;
	  
	  System.out.println("Insgesamte Programmdurchläufe: " + programmdurchlaeufe);
   
	/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  char menuePunktWahl;
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  menuePunktWahl = 'C';

	  System.out.println("Ausgewählter Menüpunkt: " + menuePunktWahl);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  final long astronomischeBerechnung;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  astronomischeBerechnung = 299792;
			  
	System.out.println("Lichtgeschwindigkeit: "  + astronomischeBerechnung + "km/s");

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	long vereinsMitglieder;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	vereinsMitglieder = 7l;
	
	System.out.println(vereinsMitglieder + " Vereinsmitglieder");
	
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	final String elektrischeElementarladung = "1,602⋅10−19As";
	
	System.out.println("Die elektrische Elementarladung beträgt: " + elektrischeElementarladung);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	boolean erfolgteZahlung;
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	erfolgteZahlung = true;
	
	System.out.println("Zahlung erfolgt: " + erfolgteZahlung);
  }//main
}// Variablen