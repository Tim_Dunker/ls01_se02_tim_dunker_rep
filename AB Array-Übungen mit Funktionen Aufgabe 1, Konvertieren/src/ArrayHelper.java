import java.util.Arrays;
public class ArrayHelper {

	public static void main(String[] args) {
		
		int[] zahlen = { 1, 2, 3, 4 ,5 };
		String convertedString = convertArrayToString(zahlen);
		System.out.println(convertedString);
	}
	public static String convertArrayToString (int[] zahlen) {
		
		String outputString = Arrays.toString(zahlen);
		return outputString;
	}
}
