import java.util.Scanner;

public class ABSchleifen2Aufgabe5 {

	public static void main(String[] args) {
		Scanner key = new Scanner(System.in);

		System.out.printf("Laufzeit ( in Jahren) des Sparvertrags: ");
		double laufzeit = key.nextDouble();

		System.out.printf("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double kapital = key.nextDouble();
		double eingezahlterBetrag = kapital;

		System.out.printf("Zinssatz: ");
		double zinssatz = key.nextDouble();

		double ausgezahlteskapital = kapital;
		double rechnungszeit = 0;
		while (rechnungszeit != laufzeit) {
			ausgezahlteskapital = ausgezahlteskapital * (1 + zinssatz / 100);
			rechnungszeit++;

		}

		System.out.println("Eingezahltes Kapital: " + eingezahlterBetrag + " Euro");
		System.out.printf("Ausgezahltes Kapital: %.2f Euro %n", ausgezahlteskapital);

		key.close();
	}
}
