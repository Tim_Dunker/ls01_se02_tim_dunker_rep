
public class Berechnung {

	public static void main(String[] args) {
		for(int bla=0; bla<10; bla++) {
			System.out.println(""+ bla + "!=" + fac_rekursiv(bla));
			System.out.println(""+ bla + "!=" + fac_iterativ(bla));
		}
	}

	

	private static int fac_rekursiv(int n) {
		if(n==0) return 1;
		else return fac_rekursiv(n-1)*n;
	}

	private static int fac_iterativ(int n) {
		int f = 1;
		for(int i=2; i<=n; i++) {
			f = f*i;
		}
		return f;
	}

}
