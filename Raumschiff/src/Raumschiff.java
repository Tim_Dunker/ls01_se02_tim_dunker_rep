import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsliste;
	private ArrayList<String> broadcastKommunikator;

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "N/A";
		this.ladungsliste = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<String> ladungsliste, ArrayList<String> broadcastKommunikator) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.ladungsliste = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public ArrayList<Ladung> getLadungsliste() {

		return ladungsliste;
	}

	public void addLadung(Ladung ladung) {
		ladungsliste.add(ladung);
		System.out.println("Ladung hinzugefügt: " + ladung);
	}

	public void photonentorpedoSchiessen(Raumschiff zielSchiff) {
		String waffenArt = "Photonentorpedo";
		if (photonentorpedoAnzahl == 0) {
			photonentorpedoNachladen(waffenArt);
		}
		if (photonentorpedoAnzahl >= 1) {
			System.out.println("Photonentorpedo abgeschossen");
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			zielSchiff.trefferVermerken(waffenArt);
		} else {
			System.out.println("-=*Click*=-");
		}
	}

	private void photonentorpedoNachladen(String waffenArt) {
		for (Ladung ladung : ladungsliste) {
			if (ladung.getBezeichnung().equals(waffenArt)) {
				int anzahlEntnommen = ladung.ladungEntnehmen(1);
				photonentorpedoAnzahl += anzahlEntnommen;
				if (anzahlEntnommen > 0)
					ladungAufraeumen();
				return;
			}
		}

	}

	public void phaserkanoneSchiessen(Raumschiff zielSchiff) {
		String waffenArt = "Phaserkanone";
		if (energieversorgungInProzent >= 50) {
			System.out.println("Phaserkanone abgeschossen");
			energieversorgungInProzent = energieversorgungInProzent - 50;
			zielSchiff.trefferVermerken(waffenArt);
		} else if (energieversorgungInProzent <= 50) {
			System.out.println("-=*Click*=-");

		}
	}

	public void trefferVermerken(String waffenArt) {
		if (getHuelleInProzent() <= 0) {
			System.out.println("Das Schiff ist bereits außer gefecht");
			return;
		} else {
			System.out.println(getSchiffsname() + " wurde getroffen!");
			broadcastKommunikator.add(getSchiffsname() + " wurde getroffen mit " + waffenArt);
			if (getSchildeInProzent() > 0) {
				setSchildeInProzent(getSchildeInProzent() - 50);
			} else if (getSchildeInProzent() <= 0 && getHuelleInProzent() > 0) {
				setHuelleInProzent(getHuelleInProzent() - 50);
				setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			}
			if (getHuelleInProzent() <= 0) {
				setLebenserhaltungssystemeInProzent(0);
				System.out.println("Die Lebenserhaltunssysteme wurden Zerstört!");
			}
		}

	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void reparaturDurchfuehren(boolean schildeBeschädigt, boolean energieversorgungBeschädigt,
			boolean huelleBeschädigt, int angeforderteAndroiden) {

		int eingesetzteAndroiden = 0;
		int schadenZaehler = 0;

		if (schildeBeschädigt = true) {
			schadenZaehler++;
		}
		if (energieversorgungBeschädigt = true) {
			schadenZaehler++;
		}
		if (huelleBeschädigt = true) {
			schadenZaehler++;
		}

		Random r = new Random();
		int tiefsteZahl = 0;
		int hoechsteZahl = 100;
		int resultat = r.nextInt(hoechsteZahl - tiefsteZahl) + tiefsteZahl;

		if (angeforderteAndroiden > androidenAnzahl) {
			eingesetzteAndroiden = androidenAnzahl;
		} else if (angeforderteAndroiden <= androidenAnzahl) {
			eingesetzteAndroiden = angeforderteAndroiden;
		}

		int berechneteReparaturInProzent = (resultat * eingesetzteAndroiden) / schadenZaehler;

		if (schildeBeschädigt = true) {
			if (berechneteReparaturInProzent + this.getSchildeInProzent() > 100) {
				this.setSchildeInProzent(100);
			} else
				this.setSchildeInProzent(berechneteReparaturInProzent + this.getSchildeInProzent());
		}
		if (huelleBeschädigt = true) {
			if (berechneteReparaturInProzent + this.getHuelleInProzent() > 100) {
				this.setHuelleInProzent(100);
			} else
				this.setHuelleInProzent(berechneteReparaturInProzent + this.getHuelleInProzent());
		}
		if (energieversorgungBeschädigt = true) {
			if (berechneteReparaturInProzent + this.getEnergieversorgungInProzent() > 100) {
				this.setEnergieversorgungInProzent(100);
			} else
				this.setEnergieversorgungInProzent(berechneteReparaturInProzent + this.getEnergieversorgungInProzent());
		}

	}

	public void zustandRaumschiff() {
		System.out.println("Schiff: " + getSchiffsname());

		System.out.println("Photonentorpedos: " + getPhotonentorpedoAnzahl());
		System.out.println("Zustand der Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Zustand der Schilde: " + getSchildeInProzent() + "%");
		System.out.println("Zustand der Hülle: " + getHuelleInProzent() + "%");
		System.out.println("Zustand der Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Anzahl der Androiden: " + getAndroidenAnzahl());

	}

	private void ladungAufraeumen() {
		ArrayList<Ladung> neueLadungsliste = new ArrayList<Ladung>();
		for (Ladung ladung : ladungsliste) {
			if (ladung.getMenge() > 0) {
				neueLadungsliste.add(ladung);
			}
		}
		ladungsliste = neueLadungsliste;
	}
}
