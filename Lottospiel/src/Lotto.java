
public class Lotto {

	public static void main(String[] args) {

		
		int[] lottozahlen = { 3, 7, 12, 18, 37, 42 };
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print(lottozahlen[i] + " ");
		}
		System.out.println(" ");
		lottotest(lottozahlen, 12);
		lottotest(lottozahlen, 13);
	}

	public static void lottotest(int[] lottozahlen, int pr�fzahl) {
		int z�hler = 0;
		boolean pr�fung = false;
		while (z�hler < lottozahlen.length && pr�fung == false) {
			if (lottozahlen[z�hler] == pr�fzahl) {
				pr�fung = true;
				z�hler++;
			} else {
				pr�fung = false;
				z�hler++;
			}
		}
		if (pr�fung == true) {
			System.out.println("Die Zahl " + pr�fzahl + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + pr�fzahl + " ist in der Ziehung nicht enthalten.");
		}

	}
}